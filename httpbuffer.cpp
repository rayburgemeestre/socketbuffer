#include "httpbuffer.h"

#include <sstream>

size_t tosize_t(const std::string &s) 
{
	std::istringstream f(s);
	size_t ans;
	f >> ans;
	// validation omitted
	return ans;
}

httpbuffer::httpbuffer()
	: expected_length_(0),
	  observed_length_(0),
	  content_length_detected_(false),
	  start_observing_length_(false)
{
}

httpbuffer::~httpbuffer()
{
}

bool httpbuffer::parse_line()
{
	// Only exception is that if we are buffering while knowing the content-length, that we may serve from buffer without
	//  it being a complete line..
	size_t total_observed = already_served_length_ + buffer_.length();
	if (start_observing_length_ && total_observed >= expected_length_) {
		std::string incomplete_but_complete_line = buffer_.substr(0, expected_length_ - already_served_length_);
		buffer_ = buffer_.substr(expected_length_ - already_served_length_);
		parsed_chunks_.push_front("LINE");
		parsed_chunks_.push_front(incomplete_but_complete_line);

		content_length_detected_ = start_observing_length_ = false;
		return true;
	}

	if (!has_line()) {
		return false;
	}

	std::string line = get_line();

	// See if we have a CONNECT request
	size_t pos = line.find(" ");
	if (pos != std::string::npos && ! start_observing_length_) { // Note the second part of this statement, it means that if we are pro-
	                                                             // cessing content payload, we shouldn't attempt to parse for connects
		std::string method = line.substr(0, pos);
		std::string address, request, domain, port;
		bool isMethodConnect = method == "CONNECT";
		bool isMethodOk = method == "GET" || method == "POST" || method == "PUT" || method == "DELETE" || method == "HEAD" || method == "OPTIONS";
		bool isRequestForConnect = isMethodConnect;
		if (isMethodConnect) {
			int len = method.length() + 1;
			address  = line.substr(len, line.find(" ", len) - len);
			domain = address;
			port = "80";
			size_t colonPos = address.find(":");
			if (colonPos != std::string::npos) {
				domain = address.substr(0, colonPos);
				port = address.substr(colonPos + 1);
			}
		}
		else if (isMethodOk) {
			size_t pos = line.find(method + " http://");
			if (pos == std::string::npos) {
				pos = line.find(method + " HTTP://");
			}
			size_t len = method.length() + 1 /* for the separating space , "GET " */;
			if (pos != std::string::npos) {
				len += std::string("HTTP://").length();
			}
			address  = line.substr(len, line.find("/", len) - len);
			isRequestForConnect = !address.empty();
			request = std::string(method + " ") + line.substr(line.find("/", len));
			domain = address;
			port = "80";
			size_t colonPos = address.find(":");
			if (colonPos != std::string::npos) {
				domain = address.substr(0, colonPos);
				port = address.substr(colonPos + 1);
			}
		}
		if (isRequestForConnect) {
			parsed_chunks_.push_front("CONNECT");
			parsed_chunks_.push_front(domain);
			parsed_chunks_.push_front(port);
			if (!request.empty()) {
				parsed_chunks_.push_front("LINE");
				parsed_chunks_.push_front(request);
			}
			return true;
		}
	}

	// Otherwise treat as a line of data
	parsed_chunks_.push_front("LINE");
	parsed_chunks_.push_front(std::string(line.c_str(), line.length()));
	if (start_observing_length_) {
		already_served_length_ += line.length();
	}
	std::string needle("Content-Length: ");
	if (line.find(needle) != std::string::npos) {
		expected_length_ = tosize_t(line.substr(needle.length()));
		observed_length_ = 0;
		already_served_length_ = 0;
		content_length_detected_ = true;
	}
	if (content_length_detected_ && (line.compare("\n") == 0 || line.compare("\r\n") == 0)) {
		start_observing_length_ = true;
	}
	return true;
}

std::string httpbuffer::pop()
{
	if (parsed_chunks_.empty())
		return "";

	std::string ret = parsed_chunks_.back();
	parsed_chunks_.pop_back();
	return ret;
}

void httpbuffer::append(const char *buffer)
{
	socketbuffer::append(buffer);
	if (start_observing_length_)
		observed_length_ += std::string(buffer).length();
}

void httpbuffer::append(const char *buffer, size_t buflen)
{
	socketbuffer::append(buffer, buflen);
	if (start_observing_length_)
		observed_length_ += buflen;
}
