#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "socketbuffer.h"
#include "httpbuffer.h"

#include <iostream>

TEST_CASE( "socketbuffer write  null characters", "[socketbuffer]" ) 
{
	socketbuffer buffer;

	buffer.append("GET");
	REQUIRE( buffer.length() == 3 );

	buffer.append("GET");
	REQUIRE( buffer.length() == 6 );

	// null is not recognized if you do not explicitly send the size
	buffer.append("\0ET");
	REQUIRE( buffer.length() == 6 );

	// null is supported this way
	buffer.append("\0ET", 3);
	REQUIRE( buffer.length() == 9 );
}

TEST_CASE( "socketbuffer read stored null characters", "[socketbuffer]" ) 
{
	socketbuffer buffer;

	buffer.append("GET\0XXX", 7);
	REQUIRE( buffer.length() == 7 );
	const std::string &ret = buffer.get();
	REQUIRE( ret.length() == 7 );
	REQUIRE( ret.compare(0, 7, std::string("GET\0XXX", 7)) == 0 );
}

TEST_CASE( "socketbuffer has_line detects line endings in raw buffer", "[socketbuffer]" ) 
{
	socketbuffer buffer;

	buffer.append("Hello\0 World!\n", 14);
	REQUIRE( buffer.has_line() == true );
	REQUIRE( buffer.get_line() == std::string("Hello\0 World!\n", 14) );

	socketbuffer buffer2;
	buffer2.append("Hello\0 World!\0", 14);
	REQUIRE( buffer2.has_line() == false );
	buffer2.append("\nHello me!\n", 11);
	REQUIRE( buffer2.has_line() == true );
	REQUIRE( buffer2.get_line() == std::string("Hello\0 World!\0\n", 15) );
	REQUIRE( buffer2.has_line() == true );
	REQUIRE( buffer2.get_line() == std::string("Hello me!\n", 10) );
	REQUIRE( buffer2.has_line() == false );
	REQUIRE( buffer2.get_line() == "" );
	buffer2.append("REST");
	REQUIRE( buffer2.length() == 4 );
	REQUIRE( buffer2.get_line() == "" );
	REQUIRE( buffer2.get_raw() == "REST" );
	REQUIRE( buffer2.get_raw() == "" );
	REQUIRE( buffer2.length() == 0 );
}

TEST_CASE( "socketbuffer buffering", "[socketbuffer]" ) 
{
	socketbuffer buffer;

	buffer.append("line1\nline2\nli");
	REQUIRE( buffer.get_line() == std::string("line1\n") );
	REQUIRE( buffer.get_line() == std::string("line2\n") );
	buffer.append("ne3\nline4\ndata_end.");
	REQUIRE( buffer.get_line() == std::string("line3\n") );
	REQUIRE( buffer.get_line() == std::string("line4\n") );
	REQUIRE( buffer.get_line() == std::string("") );
	REQUIRE( buffer.get_raw() == std::string("data_end.") );
}

TEST_CASE( "httpbuffer is also just a socketbuffer", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("CONNECT domain:80 HTTP/1.0\nHost: domain\n\nHello world\n");
	REQUIRE( buffer.get_line() == std::string("CONNECT domain:80 HTTP/1.0\n") );
	REQUIRE( buffer.get_line() == std::string("Host: domain\n") );
	REQUIRE( buffer.get_line() == std::string("\n") );
	REQUIRE( buffer.get_line() == std::string("Hello world\n") );
}

TEST_CASE( "httpbuffer parses connection request 1/2", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("CONNECT domain:80 HTTP/1.0\nHost: domain\n\nHello world\n");

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Hello world\n" );
}

TEST_CASE( "httpbuffer parses connection request 2/2", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("GET http://domain:80/some?nice=request HTTP/1.0\nHost: domain\n\nHello world\n");

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );

	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /some?nice=request HTTP/1.0\n" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Hello world\n" );
}

TEST_CASE( "httpbuffer handles content-length intelligently", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("CONNECT domain:80 HTTP/1.0\nHost: domain\nContent-Length: 5\nX-Header: 1\n\nHello");//GET /some-other-request HTTP/1.0\n");

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );
	REQUIRE( buffer.parse_line() == true );  // A flag should be set when Content-Length: 5 is encountered...
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Content-Length: 5\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "X-Header: 1\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );
	REQUIRE( buffer.parse_line() == true );  // parse_line() will extract without the newline anyway., thanks to the flag
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Hello" );
}

TEST_CASE( "httpbuffer handles content-length intelligently 2", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("CONNECT domain:80 HTTP/1.0\nHost: domain\nX-Header: 1\n\nHello");

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "X-Header: 1\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );
	REQUIRE( buffer.parse_line() == false );  // we didn't have a content-length, so this shouldn't force a line

	REQUIRE( buffer.pop() == "" );
	REQUIRE( buffer.pop() == "" );

}

TEST_CASE( "httpbuffer handles content-length intelligently 3", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("CONNECT domain:80 HTTP/1.0\nHost: domain\nContent-Length: 5\nX-Header: 1\n\nHelloGET /some-other-request HTTP/1.0\n");

	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );
	REQUIRE( buffer.parse_line() == true );  // A flag should be set when Content-Length: 5 is encountered...
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Content-Length: 5\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "X-Header: 1\n" );
	REQUIRE( buffer.parse_line() == true );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );
	REQUIRE( buffer.parse_line() == true );  // parse_line() will extract without the newline anyway., thanks to the flag
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Hello" );
	REQUIRE( buffer.parse_line() == true ); // and the next request..
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /some-other-request HTTP/1.0\n" );
	REQUIRE( buffer.parse_line() == false ); 
}

TEST_CASE( "httpbuffer handles content-length intelligently 4", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append("GET http://domain:80/some/request HTTP/1.0\nHost: domain\nContent-Length: 11\n\nHello=WorldGET /my.css HTTP/1.0\n");

	while (buffer.parse_line()) {}

	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "domain" );
	REQUIRE( buffer.pop() == "80" );

	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /some/request HTTP/1.0\n" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Host: domain\n" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Content-Length: 11\n" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "\n" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "Hello=World" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /my.css HTTP/1.0\n" );
}

TEST_CASE( "httpbuffer handles content-length intelligently 5", "[httpbuffer]" ) 
{
	httpbuffer buffer;

	buffer.append(
		"POST http://212.123.236.196:80/api/2.37/get_supplier_detail.json HTTP/1.1\r\n"
		"Accept: */*\r\n"
		"Proxy-Connection: Keep-Alive\r\n"
		"Host: 212.123.236.196\r\n"
		"Accept-encoding: gzip, deflate\r\n"
		"User-Agent: Zend_Http_Client\r\n"
		"Content-Type: application/x-www-form-urlencoded\r\n"
		"Content-Length: 18\r\n"
		"\r\n"
		"aanbieder_id=37021\r\n"
	);

	while (buffer.parse_line()) {}

	std::string s;
	do {
		s = buffer.pop();
		//std::cout << s << std::endl;

	} while (!s.empty());
}

TEST_CASE( "httpbuffer supports all-caps HTTP as well", "[httpbuffer]" )
{
	httpbuffer buffer;

	buffer.append("GET HTTP://212.123.236.196/api/2.37/get_supplier_detail.json HTTP/1.1\r\n");

	while (buffer.parse_line()) {}

	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "212.123.236.196" );
	REQUIRE( buffer.pop() == "80" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /api/2.37/get_supplier_detail.json HTTP/1.1\r\n" );
}

TEST_CASE( "httpbuffer supports multiple CONNECTS in one stream", "[httpbuffer]" )
{
	httpbuffer buffer;

	buffer.append("GET HTTP://212.123.236.196/api/2.37/get_supplier_detail.json HTTP/1.1\r\n");
	buffer.append("GET HTTP://212.123.236.196:1111/api/2.37/get_supplier_detail.json HTTP/1.1\r\n");

	while (buffer.parse_line()) {}

	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "212.123.236.196" );
	REQUIRE( buffer.pop() == "80" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /api/2.37/get_supplier_detail.json HTTP/1.1\r\n" );

	REQUIRE( buffer.pop() == "CONNECT" );
	REQUIRE( buffer.pop() == "212.123.236.196" );
	REQUIRE( buffer.pop() == "1111" );
	REQUIRE( buffer.pop() == "LINE" );
	REQUIRE( buffer.pop() == "GET /api/2.37/get_supplier_detail.json HTTP/1.1\r\n" );
}

TEST_CASE( "httpbuffer is not fooled by possible CONNECTS in POST data", "[httpbuffer]" )
{
	httpbuffer buffer;

	buffer.append(
		"POST http://domain/file HTTP/1.1\r\n"
		"Connection: keep-alive\r\n"
		"Content-Length: 100\r\n"   // Note that this encapsulates the following data as payload
		"\r\n"
		"POST http://domain/file HTTP/1.1\r\n"
		"Connection: keep-alive\r\n"
		"Content-Length: 18\r\n"
		"\r\n"
		"aanbieder_id=37021\r\n"
		"POST http://domain2/file HTTP/1.1\r\n" // But after that, we *DO* have a new connect !
		"Connection: keep-alive\r\n"
		"Content-Length: 18\r\n"
		"\r\n"
		"aanbieder_id=37021\r\n"
	);

	while (buffer.parse_line()) {}

	REQUIRE (buffer.pop() == "CONNECT");
	REQUIRE (buffer.pop() == "domain");
	REQUIRE (buffer.pop() == "80");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "POST /file HTTP/1.1\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "Connection: keep-alive\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "Content-Length: 100\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "POST http://domain/file HTTP/1.1\r\n" // here is the entire post data
							 "Connection: keep-alive\r\n"
							 "Content-Length: 18\r\n\r\n"
							 "aanbieder_id=37021\r\n");

	REQUIRE (buffer.pop() == "CONNECT"); // here is a legit second connect
	REQUIRE (buffer.pop() == "domain2");
	REQUIRE (buffer.pop() == "80");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "POST /file HTTP/1.1\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "Connection: keep-alive\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "Content-Length: 18\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "\r\n");
	REQUIRE (buffer.pop() == "LINE");
	REQUIRE (buffer.pop() == "aanbieder_id=37021");

}

