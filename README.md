# socketbuffer (and httpbuffer) helper classes

Developed test-driven using catch.hpp (https://github.com/philsquared/Catch)
Used in wxHttpProxy (http://bitbucket.org/rayburgemeestre.net/wxhttpproxy) project.

## Usage

* See test.cpp for example usage.
* Alternatively view source of wxHttpProxy.


## Build

    trigen@Firefly21 /projects/socketbuffer $ make && ./test
    g++ -g test.cpp socketbuffer.cpp httpbuffer.cpp -o test
    All tests passed (132 assertions in 12 test cases)

